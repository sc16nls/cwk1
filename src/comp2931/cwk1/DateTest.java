package comp2931.cwk1;
import java.util.Calendar;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import static org.junit.Assert.*;


public class DateTest{
  
  //Tests if the fields in the Date class are initialised correctly
  @Test
    public void dateToStringCheck() {
      Date date = new Date(2017,6,25);
      Date date2 = new Date(2014,6,24);

      assertThat(date.toString(), is("2017-06-25"));
      assertThat(date2.toString(), is("2014-06-24"));
    }

  @Test
    public void dateSingleDigitDaysValidation() {
      Date dateSingleDigitDay = new Date(0,5,4);
      Date date2SingleDigitDay = new Date(0,1,1);

      assertThat(dateSingleDigitDay.toString(), is("0000-05- 4"));
      assertThat(date2SingleDigitDay.toString(), is("0000-01- 1"));
    }

  @Test(expected = IllegalArgumentException.class)
    public void invalidZeroAndNegativeDates() {
      Date invalidDateZeroes = new Date(1,0,0);
      Date invalidDateNegativeIntegers = new Date(-1,-3,-4);
    }

  //test case that will fail the tests to check if the exception is throwen correctly
  //@Test(expected = IllegalArgumentException.class)
  //  public void validZeroAndNegativeDates() {
  //    Date validDateZeroes = new Date(0,5,4);
  //  }

  @Test(expected = IllegalArgumentException.class)
    public void invalidAndValidZeroFields() {
      Date zeroMonthAndYearValidDay = new Date(0,0,23);
      Date zeroMonthAndDayValidYear = new Date(2010,0,0);
      Date zeroYearAndDayValidMonth = new Date(0,10,0);
    }

  @Test(expected = IllegalArgumentException.class)
    public void invalidAndValidNegativeFields() {
      Date negativeMonthAndYearValidDay = new Date(-1,-2,23);
      Date negativeMonthAndDayValidYear = new Date(2010,-3,-4);
      Date negativeYearAndDayValidMonth = new Date(-1,10,-5);
    }

//to check if the method works correctly for August it gives a test fail, since the test doesn't receive an exception
//@Test(expected = IllegalArgumentException.class)
//  public void dateValidAugust() {
//    Date validAugust = new Date(2016,8,31);
//  }

  @Test(expected = IllegalArgumentException.class)
    public void invalidMonthAndDay() {
      Date invalidYear = new Date(-1,13,22);
      Date invalidMonth = new Date(2017,13,22);
      Date invalidDay = new Date(2016,3,32);
    }

  @Test(expected = IllegalArgumentException.class)
    public void invalidDates() {
      Date invalidFebruary = new Date(2015,2,29);
      Date invalidApril = new Date(2016,4,31);
      Date invalidJune = new Date(2016,6,31);
    }

  @Test(expected = IllegalArgumentException.class)
    public void invalidLeapYear() {
      Date invalidFebruaryLeapYear = new Date(2016,2,30);
    }

//a test which will fail to check if 29 february is accepted
//@Test(expected = IllegalArgumentException.class)
//  public void dateValidLeapYear() {
//    Date validFebruaryLeapYear = new Date(2016,2,29);
//  }

  //tests if the leap year boolean in the isLeapYear() method works correctly
  @Test
    public void leapYearTwenties() {
      Date leapYear = new Date(2016,6,25);
      Date anotherLeapYear = new Date(2012,6,25);

      assertThat(leapYear.isLeapYear(2016), is(true));
      assertThat(anotherLeapYear.isLeapYear(2012), is(true));
    }

  @Test
    public void leapYearNineteensAndEighteens() {
      Date nineteensLeapYear = new Date(1804,6,25);
      Date eighteensLeapYear = new Date(1912,6,25);

      assertThat(nineteensLeapYear.isLeapYear(1912), is(true));
      assertThat(eighteensLeapYear.isLeapYear(1804), is(true));
    }

  //Tests the equals() method
  @Test
    public void equalDates() {
      Date firstEqualDate = new Date(2017,5,21);
      Date secondEqualDate = new Date(2017,5,21);

      assertThat(firstEqualDate.equals(secondEqualDate), is(true));
    }

  @Test
    public void unEqualDates() {

      Date ComparableDate = new Date(2017,5,21);

      Date unEqualYear = new Date(2012,5,21);
      Date unEqualMonth = new Date(2016,7,21);
      Date unEqualDay = new Date(2016,5,24);

      assertThat(ComparableDate.equals(unEqualYear), is(false));
      assertThat(ComparableDate.equals(unEqualMonth), is(false));
      assertThat(ComparableDate.equals(unEqualDay), is(false));
    }

  @Test
    public void unEqualDaysMonthsAndYears() {

      Date ComparisonDate = new Date(2017,5,21);

      Date unEqualDayAndMonth = new Date(2012,4,19);
      Date unEqualMonthAndYear = new Date(1990,7,21);
      Date unEqualDayAndYear = new Date(1900,5,24);
      Date unEqualDayYearAndMonth = new Date(2000,1,23);

      assertThat(ComparisonDate.equals(unEqualDayAndMonth), is(false));
      assertThat(ComparisonDate.equals(unEqualMonthAndYear), is(false));
      assertThat(ComparisonDate.equals(unEqualDayAndYear), is(false));
      assertThat(ComparisonDate.equals(unEqualDayYearAndMonth), is(false));
    }

  @Test
    public void notADateObject() {
      Date ComparedDate = new Date(2017,5,21);
      assertThat(ComparedDate.equals("2017-05-21"),is(false));
    }

  //Tests the getDayOfYear() method
  @Test
    public void firstThreeMonthsOfTheYear(){
      Date firstDayOfYear = new Date(2017,1,1);
      Date firstDayOfFebruary = new Date(2017,2,1);
      Date lastDayOfFebruary = new Date(2017,2,28);
      Date firstDayOfMarch = new Date(2017,3,1);
      Date lastDayOfMarch = new Date(2017,3,31);

      assertThat(firstDayOfYear.getDayOfYear(), is(1));
      assertThat(firstDayOfFebruary.getDayOfYear(), is(32));
      assertThat(lastDayOfFebruary.getDayOfYear(), is(59));
      assertThat(firstDayOfMarch.getDayOfYear(), is(60));
      assertThat(lastDayOfMarch.getDayOfYear(), is(90));
    }

  @Test
    public void aprilMayJuneDaysOfTheYear(){
      Date firstDayOfApril = new Date(2017,4,1);
      Date lastDayOfApril = new Date(2017,4,30);
      Date firstDayOfMay = new Date(2017,5,1);
      Date lastDayOfMay = new Date(2017,5,31);
      Date firstDayOfJune = new Date(2017,6,1);
      Date lastDayOfJune = new Date(2017,6,30);

      assertThat(firstDayOfApril.getDayOfYear(), is(91));
      assertThat(lastDayOfApril.getDayOfYear(), is(120));
      assertThat(firstDayOfMay.getDayOfYear(), is(121));
      assertThat(lastDayOfMay.getDayOfYear(), is(151));
      assertThat(firstDayOfJune.getDayOfYear(), is(152));
      assertThat(lastDayOfJune.getDayOfYear(), is(181));
    }

  @Test
    public void julyAugugstSeptemberDaysOfTheYear(){
      Date firstDayOfJuly = new Date(2017,7,1);
      Date lastDayOfJuly = new Date(2017,7,31);
      Date firstDayOfAugust = new Date(2017,8,1);
      Date lastDayOfAugust = new Date(2017,8,31);
      Date firstDayOfSeptember = new Date(2017,9,1);
      Date lastDayOfSeptember = new Date(2017,9,30);

      assertThat(firstDayOfJuly.getDayOfYear(), is(182));
      assertThat(lastDayOfJuly.getDayOfYear(), is(212));
      assertThat(firstDayOfAugust.getDayOfYear(), is(213));
      assertThat(lastDayOfAugust.getDayOfYear(), is(243));
      assertThat(firstDayOfSeptember.getDayOfYear(), is(244));
      assertThat(lastDayOfSeptember.getDayOfYear(), is(273));
    }

  @Test
    public void lastThreeMonthsOfTheYear(){
      Date firstDayOfOctober = new Date(2017,10,1);
      Date lastDayOfOctober = new Date(2017,10,31);
      Date firstDayOfNovember = new Date(2017,11,1);
      Date lastDayOfNovember = new Date(2017,11,30);
      Date firstDayOfDecember = new Date(2017,12,1);
      Date lastDayOfDecember = new Date(2017,12,31);

      assertThat(firstDayOfOctober.getDayOfYear(), is(274));
      assertThat(lastDayOfOctober.getDayOfYear(), is(304));
      assertThat(firstDayOfNovember.getDayOfYear(), is(305));
      assertThat(lastDayOfNovember.getDayOfYear(), is(334));
      assertThat(firstDayOfDecember.getDayOfYear(), is(335));
      assertThat(lastDayOfDecember.getDayOfYear(), is(365));
    }

  //Tests the leapYear handeling in the getDayOfYear() method
  @Test
    public void daysOfLeapYears(){
      Date leapYearMay = new Date(1804,5,1);
      Date notLeapYearMay = new Date(2001,5,1);
      Date leapYearDecember = new Date(2016,12,31);
      Date notLeapYearDecember = new Date(2011,12,31);


      assertThat(leapYearMay.getDayOfYear(), is(122));
      assertThat(notLeapYearMay.getDayOfYear(), is(121));
      assertThat(leapYearDecember.getDayOfYear(), is(366));
      assertThat(notLeapYearDecember.getDayOfYear(), is(365));
    }

  //Tests the getDayCount() method
  @Test
    public void dayCountsTest(){
      Date dayCountThirtyDays = new Date(1804,5,30);
      Date dayCountFifteenDays = new Date(2001,5,15);
      Date dayCountTenDays = new Date(2016,12,10);


      assertThat(dayCountThirtyDays.getDayCount(30), is(30));
      assertThat(dayCountFifteenDays.getDayCount(15), is(15));
      assertThat(dayCountTenDays.getDayCount(10), is(10));
    }

  //Tests Date class default constructor initialisation
  @Test
    public void todayDate() {
      Date todayDate = new Date();

        //the fields initialised by the object created by ValidDate will not pass these tests, since it is set to give today's date
        //the fields initialised by the object created by Mock will pass these tests, since it is set to
        //check if the constructor makes an accurate object with today's date on any computer
        //so it is constant

        assertThat(todayDate.getDay(), is(28));
        assertThat(todayDate.getMonth(), is(5));
        assertThat(todayDate.getYear(), is(2017));
      }

}
