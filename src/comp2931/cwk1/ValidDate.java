package comp2931.cwk1;
import java.util.Calendar;
/**
 * Simple representation of a valid calendar current/today date.
 * implements the Validation interface
 */
public class ValidDate implements Validation{

  int validYear;
  int validMonth;
  int validDay;

  /**
   * Creates a date using the given values for validYear, validMonth and validDay.
   * uses the Calendar object and initialises the fields
   */
  public ValidDate(){

    //gets a calendar with the default timezone and locationale
    Calendar today = Calendar.getInstance();
    //gets the data from the created Calendar object
    validYear = today.get(Calendar.YEAR);
    validMonth = 1 + today.get(Calendar.MONTH);
    validDay = today.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Returns the day component of this date by overriding the getDay() method
   * in Validation
   * @return validDay of type int
   */
  @Override
    public int getDay(){
      return validDay;
    }
    /**
     * Returns the month component of this date by overriding the getMonth() method
     * in Validation
     * @return validMonth of type int
     */
  @Override
    public int getMonth(){
      return validMonth;
    }
    /**
     * Returns the year component of this date by overriding the getYear() method
     * in Validation
     * @return validYear of type int
     */
  @Override
    public int getYear(){
      return validYear;
    }

  }
