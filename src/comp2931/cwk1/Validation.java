package comp2931.cwk1;
/**
 * Simple interface to be implemented and overriden by Mock.java and ValidDate.java to show if a calendar with today's date works
 */
public interface Validation{


  /**
   * Returns the day component of this date
   * @return an integer
   */
  int getDay();

  /**
   * Returns the month component of this date
   * @return an integer
   */
  int getMonth();

  /**
   * Returns the year component of this date
   * @return an integer
   */
  int getYear();
}
