// Class for COMP2931 Coursework 1

package comp2931.cwk1;
import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Default constructor that makes an object giving today's date
   *
  */
  public Date(){

    // gets an object either from Mock which will be with set constant data to compare the date on any machine
    // Mock is set in the tests to pass, since it is the object to which we will compare the date created by ValidDate
    //
    // ValidDate is the object which will give us the actual calendar representation of today's date
    // it is currently commented so that the Mock object can be runned through the tests
    // uncomment to initialise the fields with today's date and comment the Mock object


    // Validation today = new ValidDate();
    Validation today = new Mock();

    //special case to check if the Calendar class gives an object with fields appropriate to initialise the default constructor fields
    if((today.getYear()<0||today.getYear()>9999||today.getMonth()<=0||today.getMonth()>12||today.getDay()<=0||today.getDay()>31)
        ||(((today.getMonth()%2==0&&today.getMonth()!=8&&today.getMonth()!=2)&&today.getMonth()<9)&&today.getDay()>30)
        ||((today.getMonth()==9&&today.getMonth()==11)&&today.getDay()>30)
        ||(!isLeapYear(today.getYear())&&(today.getMonth()==2&&today.getDay()>28))
        ||(isLeapYear(today.getYear())&&(today.getMonth()==2&&today.getDay()>29))){
      throw new IllegalArgumentException("Invalid date format please try again:");
    }
    else{
      year = today.getYear();
      month = today.getMonth();
      day = today.getDay();
    }
  }

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    boolean leapYear = this.isLeapYear(y);
    if((y<0||y>9999||m<=0||m>12||d<=0||d>31)
        ||(((m%2==0&&m!=8&&m!=2)&&m<9)&&d>30)
        ||((m==9&&m==11)&&d>30)
        ||(!leapYear&&(m==2&&d>28))
        ||(leapYear&&(m==2&&d>29))){
      throw new IllegalArgumentException("Invalid date format please try again:");
    }
    else{
      year = y;
      month = m;
      day = d;
    }
  }
  /**
   * Checks if the passed year is a leap year
   *
   * @param thisYear the year which you want to check
   * @return true if it is a leap year false otherwise
   */
  public boolean isLeapYear(int thisYear){

    if(thisYear%4==0&&thisYear%100!=0){
      return true;
    }
    else if(thisYear%4==0
      &&thisYear%100==0
      &&thisYear%400==0){
        return true;
    }

    return false;
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }
  /**
   * Overrides the equals method to compare two Date objects
   * If the object which is compared is not an instance of Date the method returns false
   * @param object which is of type Object
   * @return true if the objects are equal false otherwise
   */
  @Override
  public boolean equals(Object object){

    if(!(object instanceof Date)){
      return false;
    }
    Date other = (Date)object;

    if(this.getYear()==other.getYear()){
      if(this.getMonth()==other.getMonth()){
        if(this.getDay()==other.getDay()){
          return true;
        }
      }
    }
    return false;
  }
  /**
   * Gives the day of year of the Date object that calls the method
   *
   * @return dayOfYear of type int that contains the number of the day
   */
  public int getDayOfYear(){
    int dayOfYear = 0;
    int dayOfMonth = this.getDay();
    boolean leapYear = this.isLeapYear(year);
    //starts the mount decrementer from the previous mount
    int monthCount = this.getMonth()-1;
    //adds the days of January by default
    int daysPassed = 31;
    while(monthCount>1){
      if(monthCount!=2
        &&(((monthCount%2==0&&monthCount!=8)&&monthCount<9)
        ||(monthCount==9)
        ||(monthCount==11))){
        daysPassed+=30;
      }
      else if(!leapYear && monthCount==2){
        daysPassed+=28;
      }
      //handels leap years
      else if(leapYear && monthCount==2){
        daysPassed+=29;
      }
      else{
        daysPassed+=31;
      }

      monthCount--;
    }

    switch(month){
      case 1:
        dayOfYear = this.getDayCount(dayOfMonth);
        break;
      case 2:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 3:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 4:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 5:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 6:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 7:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 8:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 9:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 10:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 11:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
      case 12:
        dayOfYear = daysPassed+this.getDayCount(dayOfMonth);
        break;
    }
    return dayOfYear;
  }

  /**
   * Gives the number of days that have passed of the month of the date object that calls the method
   *
   * @param dayOfMonth which is of type int and it is the current day of the Date object that calls the method
   * @return dayCount of type int that contains the number of the days that have passed from the begining of the month to the day given
   */

  public int getDayCount(int dayOfMonth){
    int dayCount = 0;
    while(dayOfMonth>=1){
      dayOfMonth -- ;
      dayCount ++ ;
    }
    return dayCount;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

}
