package comp2931.cwk1;
import java.util.Calendar;
/**
 * Simple representation of a mock object, used to compare and test the calendar date
 * created by ValidDate.java.
 * implements the Validation interface
 */
public class Mock implements Validation{

  int mockDay = 28;
  int mockMonth = 5;
  int mockYear = 2017;

  /**
   * Returns the day component of this date by overriding the getDay() method
   * in Validation
   * @return validDay of type int
   */
  @Override
  public int getDay(){
    return mockDay;
  }

  /**
   * Returns the month component of this date by overriding the getMonth() method
   * in Validation
   * @return validMonth of type int
   */
  @Override
  public int getMonth(){
    return mockMonth;
  }

  /**
   * Returns the year component of this date by overriding the getYear() method
   * in Validation
   * @return validYear of type int
   */
  @Override
  public int getYear(){
    return mockYear;
  }

}
